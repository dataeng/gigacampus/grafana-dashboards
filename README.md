# Managing Grafana Dashboards with grafonnet and git

This repository forks https://github.com/adamwg/grafana-dashboards

adamwg/grafana-dashboards is the demo/example used in the
author's PromCon 2019 talk, "Managing Grafana Dashboards
with grafonnet and git".

